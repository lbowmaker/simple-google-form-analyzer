# Simple Google Form Analyzer

Simple python file to read a google sheet, find some data we care about and pass it to Google PaLM API to analyze the results.

Aimed to show how we could make sense of our responses.

## Getting started

1. You will need to create a Google service account (2 mins effort), create an access key, enable Google sheets API then add the e-mail that gets generated to view the file you care about. It is quite well explained in the link below (you will generate a credentials.json file from Google and use that).

https://www.datacamp.com/tutorial/how-to-analyze-data-in-google-sheets-with-python-a-step-by-step-guide

2. Create a Google PaLM key

https://ai.google.dev/tutorials/setup

3. Modify the code to the sheet you want to read:

https://gitlab.wikimedia.org/lbowmaker/simple-google-form-analyzer/-/blob/3c78b4b957fa97e7743225f02d1848200ed90bbd/simple_google_form_analyzer.py#L46

## How it works

The code assumes we are reading responses to our weekly Google form updates.

Based on this file we take the responses to the question:

_**🚧 Did anything block you or continues to block you?**_

We then ask PaLM API:

_**summarize specific challenges to 10 words or less found in this response list**_

As of April 5th this gives us the output:

**overloaded, long feedback loops, spark history, GitLab permissions**


