import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import google.generativeai as palm

# Define API key to connect to Google PaLM API
palm.configure(api_key='xxx')

# Define the scope of the Google Sheets API
SCOPE = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

# Path to the credentials JSON file obtained from Google Cloud Console
# Once you set this up, take the e-mail address generated in Google Cloud
# and add to view permissions of the file
CREDENTIALS_FILE = 'credentials.json'

def read_google_sheet(sheet_url):
    """Reads data from a Google Sheet and returns a DataFrame."""
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, SCOPE)
    client = gspread.authorize(credentials)
    sheet = client.open_by_url(sheet_url)
    worksheet = sheet.get_worksheet(0)
    data = worksheet.get_all_records()
    return pd.DataFrame(data)

def create_prompt(df):
    """Creates a prompt for the model based on DataFrame."""
    blocking_list = list(df['🚧 Did anything block you or continues to block you?'])
    return "summarize specific challenges to 10 words or less found in this response list:" + ", ".join(blocking_list)

def call_model(prompt):
    """Calls the model to generate a text response."""
    completion = palm.generate_text(
        model='models/text-bison-001',
        prompt=prompt,
        temperature=0,
        # The maximum length of the response
        max_output_tokens=800,
    )

    return completion.result

def main():
    """Main function to execute the program."""
    sheet_url = 'xxx'
    google_sheet_df = read_google_sheet(sheet_url)
    prompt = create_prompt(google_sheet_df)
    model_response = call_model(prompt)
    print(model_response)

if __name__ == "__main__":
    main()
